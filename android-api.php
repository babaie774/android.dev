<?php
/*
Plugin Name: android-api
Plugin URI: http://7learn.com/
Description: Android Course Api Plugin.
Version: 1.0.0
Author: 7learn.com
Author URI: http://7learn.com/
*/
define( 'API_DIR', plugin_dir_path( __FILE__ ) );
define( 'API_ROUTES_DIR', API_DIR . 'routes' . DIRECTORY_SEPARATOR );
define( 'API_REPOSITORY_DIR', API_DIR . 'repositories' . DIRECTORY_SEPARATOR );
define( 'API_CLASS_DIR', API_DIR . 'classes' . DIRECTORY_SEPARATOR );
//include "classes/Product.php";
//include "classes/UrlParser.php";
include "vendor/autoload.php";
spl_autoload_register( 'autoload' );
function autoload( $class ) {
	$classNameParts = explode( '\\', $class );
	$classPath      = implode( '/', $classNameParts );
	$classPath      .= '.php';
	$classPath      = API_DIR . $classPath;
	if ( file_exists( $classPath ) ) {
		include_once $classPath;
	}
}

add_action( 'parse_request', 'Application\Services\UrlParser::parse' );

