<?php

namespace Application\Repositories;


class OrderRepository extends BaseRepository {
	public function __construct() {
		parent::__construct();
		$this->table      = $this->db->prefix . 'orders';
		$this->primaryKey = 'order_id';
		$this->perPage    = 1;
	}

}