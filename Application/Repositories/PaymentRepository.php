<?php

namespace Application\Repositories;


class PaymentRepository extends BaseRepository {

	public function __construct() {
		parent::__construct();
		$this->table      = $this->db->prefix . 'payments';
		$this->primaryKey = 'payment_id';
	}
}