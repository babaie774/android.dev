<?php
namespace Application\Repositories;


class OrderItemRepository extends BaseRepository {

	public function __construct() {
		parent::__construct();
		$this->table = $this->db->prefix.'order_items';
		$this->primaryKey = 'order_item_id';
		$this->perPage = 50;
	}
}