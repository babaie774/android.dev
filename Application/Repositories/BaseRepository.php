<?php

namespace Application\Repositories;

abstract class BaseRepository {

	protected $table;

	protected $primaryKey;

	protected $db;

	protected $perPage = 10;

	public function __construct() {
		global $wpdb;
		$this->db = $wpdb;
	}

	public function create( array $data, array $format ) {
		$this->db->insert( $this->table, $data, $format );

		return $this->db->insert_id;
	}

	public function all( array $columns = null ) {
		$columns = is_null( $columns ) ? '*' : implode( ',', $columns );

		return $this->db->get_results( "SELECT {$columns} FROM {$this->table}" );
	}

	public function paginate( $page = 1, $order, $orderBy ) {
		$offset  = ( $page - 1 ) * $this->perPage;
		$order   = ! is_null( $order ) ? $order : $this->primaryKey;
		$orderBy = ! is_null( $orderBy ) ? $orderBy : "DESC";

		return $this->db->get_results( "SELECT * FROM {$this->table} ORDER BY {$order} {$orderBy} LIMIT $offset,{$this->perPage} " );
	}

	public function find( int $id ) {
		return $this->db->get_row( "SELECT * FROM {$this->table} WHERE {$this->primaryKey}={$id} LIMIT 1" );
	}

	public function findBy( array $criteria, $single = false ) {
		$sql_query = "SELECT * FROM {$this->table} WHERE ";
		foreach ( $criteria as $key => $value ) {
			$value     = $this->formatData( $value );
			$sql_query .= "{$key}={$value} AND";
		}
		$sql_query = rtrim( $sql_query, "AND" );
		if ( $single ) {
			$sql_query .= " LIMIT 1 ";
		}

		return $this->db->get_row( $sql_query );
	}

	public function delete( int $id ) {
		return $this->db->delete( $this->table, [ $this->primaryKey => $id ], [ '%d' ] );
	}

	public function deleteBy( array $criteria, array $format ) {
		return $this->db->delete( $this->table, $criteria, $format );
	}

	public function update( int $id, array $data, array $format ) {
		return $this->db->update( $this->table, $data, [ $this->primaryKey => $id ], $format, [ '%d' ] );
	}

	private function formatData( $data ) {
		$result = null;
		switch ( true ) {
			case is_int( $data ):
				$result = intval( $data );
				break;
			case is_string( $data ):
				$result = "'{$data}'";
				break;
			case is_bool( $data ):
				$result = $data ? 1 : 0;
				break;
			default:
				$result = $data;
				break;
		}

		return $result;
	}

}