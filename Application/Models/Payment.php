<?php

namespace Application\Models;


use Application\Repositories\OrderRepository;
use Application\Repositories\PaymentRepository;

class Payment {
	const UNPAID = 0;
	const PAID = 1;

	public static function createPayment() {
		$order_id           = $_POST['order_id'];
		$bank               = $_POST['bank'];
		$payment_repository = new PaymentRepository();
		if ( intval( $order_id ) > 0 ) {

			$order_repository = new OrderRepository();
			$order_item       = $order_repository->find( $order_id );
			$payment_code     = self::createPaymentCode();
			$paymentData      = [
				'payment_order_id'   => $order_id,
				'payment_user_id'    => (int) $order_item->order_user_id,
				'payment_amount'     => $order_item->order_payable_price,
				'payment_res_num'    => self::createResNum( $order_item->order_user_id ),
				'payment_created_at' => date( 'Y-m-d H:i:s' ),
				'payment_bank'       => $bank,
				'payment_code'       => $payment_code
			];

			$new_payment = $payment_repository->create( $paymentData, [
				'%d',
				'%d',
				'%d',
				'%s',
				'%s',
				'%s',
				'%s'
			] );

			if ( $new_payment ) {
				return [
					'status'       => true,
					'message'      => '',
					'payment_code' => $payment_code
				];
			}

			return [
				'status'  => false,
				'message' => 'failed',
			];

		}
	}

	public static function createPaymentCode() {
		return bin2hex( random_bytes( 5 ) );
	}

	public static function createResNum( $user_id ) {
		return random_int( 100000, 9999999 ) . $user_id;
	}

}