<?php

namespace Application\Models;

use Application\Repositories\ProductRepository;
use Application\Transformers\ProductTransformers;

class Product {

	public function getProducts() {
		$order   = isset( $_POST['order'] ) ? $_POST['order'] : null;
		$orderBy = isset( $_POST['order_by'] ) ? $_POST['order_by'] : null;
		$page    = isset( $_POST['page'] ) && intval( $_POST['page'] ) > 0 ? intval( $_POST['page'] ) : 1;

		$repository          = new ProductRepository();
		$products            = $repository->paginate( $page, $order, $orderBy );
		$productTransformers = new ProductTransformers();

		return $productTransformers->collection( $products );
	}

	public static function getProduct( int $product_id ) {
		$repository = new ProductRepository();

		return $repository->find( $product_id );
	}
}