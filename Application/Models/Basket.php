<?php

namespace Application\Models;


use Application\Repositories\BasketRepository;
use Application\Repositories\ProductRepository;

class Basket {

	const INCREASE = 1;
	const DECREASE = 2;

	public static function addItem() {

		$product_id = intval( $_POST['product_id'] );
//		$count      = intval( $_POST['count'] );

		if ( $product_id > 0 ) {

			if ( self::isItemExists( $product_id ) ) {
				$result = self::updateBasketItemCount( $product_id );
				if ( $result ) {
					return [
						'status'  => true,
						'message' => 'item added.'
					];
				}

				return [
					'status'  => false,
					'message' => 'failed'
				];
			} else {
				$product_repository = new ProductRepository();
				$basket_repository  = new BasketRepository();
				$product_item       = $product_repository->find( $product_id );
				if ( ! is_null( $product_item ) ) {
					$basket_item_data = array(
						'basket_item_product_id' => $product_id,
						'basket_item_count'      => 1,
						'basket_item_price'      => (int) $product_item->product_price,
						'basket_item_discount'   => (int) $product_item->basket_item_discount
					);
					$insert_result    = $basket_repository->create( $basket_item_data, [
						'%d',
						'%d',
						'%d',
						'%d'
					] );
					if ( $insert_result > 0 ) {
						return [
							'status'  => true,
							'message' => 'item added.'
						];
					}

					return [
						'status'  => false,
						'message' => 'error for add basket item.'
					];

				}

				return [
					'status'  => false,
					'message' => 'invalid product id'
				];
			}

		}

		return [
			'status'  => false,
			'message' => 'invalid product id'
		];


	}

	public static function removeItem() {
		$product_id = intval( $_POST['product_id'] );
		if ( intval( $product_id ) > 0 ) {
			if ( self::isItemExists( $product_id ) ) {
				$basket_repository = new BasketRepository();
				$result            = $basket_repository->deleteBy( [
					'basket_item_product_id' => $product_id
				], [
					'%d'
				] );
				if ( $result ) {
					return [
						'status'  => true,
						'message' => 'item deleted.'
					];
				}

				return [
					'status'  => false,
					'message' => 'failed'
				];

			}

			return [
				'status'  => false,
				'message' => 'invalid product id'
			];
		}

		return [
			'status'  => false,
			'message' => 'invalid product id'
		];
	}

	public static function isItemExists( int $product_id ) {
		$basket_repository = new BasketRepository();
		$result            = $basket_repository->findBy( [ 'basket_item_product_id' => $product_id ], true );

		return ! is_null( $result ) && intval( $result->basket_item_id ) > 0;
	}

	public static function updateBasketItemCount( int $product_id, int $type = self::INCREASE, int $count = 1 ) {
		$basket_repository = new BasketRepository();

		return $basket_repository->updateCount( $product_id, $type, $count );


	}

	public static function getBasket() {
		$basket = [
			'total_price'    => 0,
			'total_discount' => 0,
			'items_count'    => 0,
			'shipping_cost'  => 0,
			'cart_items'     => []
		];

		$basket_repository = new BasketRepository();
		$all_items         = $basket_repository->getAllItems();
		if ( $all_items && count( $all_items ) > 0 ) {
			$basket['items_count'] = count($all_items);
			foreach ( $all_items as $item ) {
				$basket_item = [
					'id'              => (int) $item->basket_item_id,
					'product_id'      => (int) $item->basket_item_product_id,
					'title'           => $item->product_title,
					'count'           => (int) $item->basket_item_count,
					'primary_price'   => (int) $item->basket_item_price,
					'sub_total_price' => (int) $item->basket_item_price * (int) $item->basket_item_count,
					'payable_price'   => (int) $item->basket_item_price - (int) $item->basket_item_discount
				];

				$basket['total_price']    += $basket_item['sub_total_price'];
				$basket['total_discount'] += (int) $item->basket_item_discount;
//				$basket['items_count']    += (int) $item->basket_item_count;
				//$basket['shipping_cost'] = 0;
				$basket['cart_items'][] = $basket_item;
			}
		}

		return $basket;
	}
}