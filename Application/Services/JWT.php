<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/26/2017
 * Time: 3:44 PM
 */

namespace Application\Services;


use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;

class JWT {

	private $parser;
	private $builder;
	private $token;

	public function __construct() {
		$this->parser  = new Parser();
		$this->builder = new Builder();
	}

	public function parse( string $data ) {
		$this->token = $this->parser->parse( $data );
	}

	public function getHeaders() {
		return $this->token->getHeaders();
	}

	public function getHeader( string $name ) {
		return $this->token->getHeader( $name );
	}

	public function getClaims() {
		return $this->token->getClaims();
	}

	public function getClaim( string $name ) {
		return $this->token->getClaim( $name );
	}

}