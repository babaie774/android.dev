<?php
namespace Application\Services;
class UrlParser {
	public static function parse() {
		$currentRoute = self::getCurrentUrl();
		if ( self::isCurrentRouteDefined() ) {
			$target = self::getRouteTarget($currentRoute);
			$result = call_user_func($target,1);
			wp_send_json($result);
			exit;
		}

	}

	public static function getCurrentUrl() {
		return strtok($_SERVER['REQUEST_URI'],'?');
	}

	public static function getRoutes() {
		$routes = include API_ROUTES_DIR . 'api.php';

		return $routes;
	}

	public static function isCurrentRouteDefined() {
		$currentRoute = self::getCurrentUrl();

		return self::isRouteDefined( $currentRoute );
	}

	public static function getRouteTarget( string $route ) {
		$routes = self::getRoutes();
		if ( self::isRouteDefined( $route ) ) {
			return $routes[ $route ];
		}
	}

	public static function isRouteDefined( $route ) {
		$routes = self::getRoutes();
		return in_array( $route, array_keys( $routes ) );
	}
}
