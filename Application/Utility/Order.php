<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/16/2017
 * Time: 6:51 PM
 */

namespace Application\Utility;


class Order {

	const FAILED = 1;
	const PENDING = 2;
	const PAID = 3;
	const DELIVERED = 4;

	const COD = 1;
	const ONLINE = 2;

}