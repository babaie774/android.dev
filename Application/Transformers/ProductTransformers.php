<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/30/2017
 * Time: 3:54 PM
 */

namespace Application\Transformers;


class ProductTransformers implements Transformer {
	public function item( $product ) {
		$product->product_price    /= 10;
		$product->product_discount /= 10;

		return [
			"product_id"             => $product->product_id,
			"product_title"          => $product->product_title,
			"product_image_urls"     => $product->product_image_urls,
			"product_current_price"  => ((string)( $product->product_price - $product->product_discount))." تومان",
			"product_previous_price" => ((string)$product->product_price)." تومان",
			"product_status"         => $product->product_status
		];
	}

	public function collection( $products ) {
		$result = array();
		foreach ( $products as $product ) {
			$result[] = $this->item( $product );
		}

		return $result;
	}
}