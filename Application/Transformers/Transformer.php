<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/30/2017
 * Time: 4:07 PM
 */

namespace Application\Transformers;


interface Transformer {
	public function item($item);
	public function collection($items);
}